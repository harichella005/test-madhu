import { useState } from "react";

const App = () => {
  var username = "Hariharan";
  let time = new Date().toLocaleTimeString();

  const [ctime, setTime] = useState(time);
  const UpdateTime = () => {
    time = new Date().toLocaleTimeString("en-IN",{
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
        hour12: true
    });
    setTime(time);
  };
  setInterval(UpdateTime);
  return <h1 className="time">{ctime} {ctime < 12 ? "Good Morning" : "Good Morning"} {username} {ctime >= 15 ? "Good evening" : "" }</h1>;
};
export default App;