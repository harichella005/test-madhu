import "./App.css";
const App = () => {
    //Auto update the year in footer area
    let year = new Date().getFullYear();
    return (
        <div>
            <h1 className="auto-update-year">&#169; Copyright {year} <span className=""><a classname="" href="https://google.com">IntechWings</a></span> All rights reserved.</h1>
        </div>
    )
};
export default App;

