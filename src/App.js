import './App.css';
//import 'bootstrap/dist/css/bootstrap.min.css';
//import 'bootstrap/dist/js/bootstrap.bundle.min';
//import Clock from "./clock";
import Copyright from "./copyright";
import React from "react";
import Typewriter from "typewriter-effect";
import { useEffect } from "react";
import Aos from "aos";
import "aos/dist/aos.css";
import { SocialIcon } from "react-social-icons";
import { MouseParallaxContainer, MouseParallaxChild } from "react-parallax-mouse";
//import { Outlet, Link } from "react-router-dom";
//import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import About from "./pages/About";
import Contact1 from "./pages/Contact";
//import Beta from './pages/blink';
import LI from "./assets/li.png"

function App() {
  useEffect(() => {
    Aos.refresh();
    Aos.init({ duration: 1000, 
    easing: 'ease-in-sine',
    once: true,
    mirror: false});
  }, []);
  //const navigate = useNavigate();
  return (
    <div className="App">
      <header className="App-header">
        <div class="whoami"></div>
        <div className='header' id="myHeader">
          <img src={LI} className="profileLogo" alt="My profile pic"></img>
          <h1 className='logoname'>Hariharan C</h1>
            <ul class="menu">
              <li>
                <a className="hbutton" href="/">Home</a>
              </li>
              <li>
              <a className="hbutton" href="/About" onClick={About}>About</a>
              </li>
              <li>
              <a className="hbutton" href="https://www.google.com/">Services</a>
              </li>
              <li>
              <a className="hbutton" href="/Contact" onClick={Contact1}>Contact</a>
              </li>
              <li>
              <a className="hbutton" href="/Contact" onClick={Contact1}>SigIn</a>
              </li>

            </ul>
            {/* <main>
              <nav class="navbar navbar-dark" aria-label='First navbar example'>
                <div class="container-fluid">
                  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample01" arica-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>

                  <div class="collapse navbar-collapse" id="navbarsExample01">
                    <ul class="navbar-nav me-auto mb-2">
                      <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Home</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#">About</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="#">Courses</a>
                      </li>
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown" aria-expanded="false">Dropdown</a>
                        <ul class="dropdown-menu">
                          <li><a class="dropdown-item" href="#">Action</a></li>
                          <li><a class="dropdown-item" href="#">Another action</a></li>
                          <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                      </li>
                    </ul>
                    <form role="search">
                      <input class="form-control" type="search" placeholder="Search" aria-label="Search"></input>
                    </form>
                  </div>
                </div>
              </nav>
            </main> */}
        </div>
        <BrowserRouter>
          <Routes>
            <Route>
              <Route path="about" element={<About />} />
              <Route path="contact" element={<Contact1 />} /> 
            </Route>
          </Routes>
        </BrowserRouter>
      </header>
      <section data-aos="flip-right" className="profilesec">
        <div>
          <h1 class="text-red blink-hard">Beta-Version</h1>
        </div>
        <div id="mv-img" className='full-profile'>
            <MouseParallaxContainer>
              <MouseParallaxChild
                factorX={0.03}
                factorY={0.03}
                updateStyles={{
                  filter: "invert(1)",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  width: "auto",
                  height: "10vh"
                }}
              >
                <img id="inner" src="https://raw.githubusercontent.com/hariharan005/hariharan/main/hari.png" className="profile" alt="My profile pic"/>
              </MouseParallaxChild>
              <h1 className="myname" data-aos="fade-right">Hariharan C</h1>
            </MouseParallaxContainer>
            {/* <img id="inner" src="https://raw.githubusercontent.com/hariharan005/hariharan/main/hari.png" className="profile" alt="My profile pic"/> */}
            {/* <h2 className="im" data-aos="slide-right">I'M</h2> */}
          </div>
          <h2 class="im"> Hello</h2>
          <div className="typing">
            <h1 class="im">I'm a</h1>
            <Typewriter 
            
            options={{
              strings: ['Web Developer', 'GUI/CLI Designer', 'Ethical Hacker', 'WebApps Pentester', ],
              autoStart: true,
              loop: true,
              
            }}
            />
        </div>
      </section>
      <section className="high" id="bio">
        <div>
        {/* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span className='start'>H</span> */}
          <p className='start' data-aos="fade-right">Hello, Myself Hariharan C and im a 22 year old Computer 
          science engineer who fasinated about programming and have keen knowledge in it, 
          after understanding the indepth knowledge of computer and technology. 
          While googling about the programming i came to know a word Bug Hunting, 
          Yes the word itself feels something intresting and before braking the system we 
          have to know about the computer system and how the technology is working.</p>
          <p data-aos="fade-left">Lorem Ipsum is simply dummy text of the printing and typesetting industry.
          Lorem Ipsum has been the industry's standard dummy text ever since the
          1500s, when an unknown printer took a galley of type and scrambled it to
          make a type specimen book. It has survived not only five centuries, but
          also the leap into electronic typesetting, remaining essentially
          unchanged. It was popularised in the 1960s with the release of Letraset
          sheets containing Lorem Ipsum passages, and more recently with desktop
          publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        </div>
      </section>
      {/* <section className="high" id="bio">
        <div data-aos="fade-right">
          <p className='start'>Hello, Myself <span className="name">Hariharan C</span> and im a 22 year old Computer science engineer who fasinated about programming and have keen knowledge in it, after understanding the indepth knowledge of computer and technology. While googling about the programming i came to know a word Bug Hunting, Yes the word itself feels something intresting and before braking the system we have to know about the computer system and how the technology is working.</p>
        </div>
      </section>       */}
      <section className="hof">
        <p className="hofname" data-aos="slide-up">Hall of Fame</p>
        <div className="grid-container grid-container-fit">
          <div className="grid-element">
            <img data-aos="flip-right" className="lifeomichof" src="https://raw.githubusercontent.com/hariharan005/hariharan/main/courier.png" alt=""/>
          </div>
          <div className="grid-element">
            <img data-aos="flip-right" className="lifeomichof" src="https://raw.githubusercontent.com/hariharan005/hariharan/be9b767015957329fe14674b793dcd7b71507b83/centrify.svg" alt=""/>
          </div>
          <div className="grid-element">
            <img data-aos="flip-right" className="lifeomichof" src="https://raw.githubusercontent.com/hariharan005/hariharan/be9b767015957329fe14674b793dcd7b71507b83/tripadvisor.svg" alt=""/>
          </div>
          <div className="grid-element">
            <img data-aos="flip-right" className="lifeomichof" src="https://raw.githubusercontent.com/hariharan005/hariharan/be9b767015957329fe14674b793dcd7b71507b83/lifeomic.svg" alt=""/>
          </div>
        </div>
      </section>
      <section className="knowledge" data-aos="fade-up">
        <h1 class="pltitle">Programming Languages: </h1>
        <div className="pl">
          <div className="tooltip">
            <a href="https://www.python.org/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/python/python-original.svg" alt="lang" />
            <span className="tooltipText">Python</span></a>
          </div>
          <div className="tooltip">
            <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/go/go-original.svg" alt="lang" />
            <span className="tooltipText">Go</span></a>
          </div>
          <div className="tooltip">
            <a href="https://www.w3schools.com/java/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/java/java-original-wordmark.svg" alt="lang" />
            <span className="tooltipText">Java</span></a>
          </div>
          <div className="tooltip">
            <a href="https://www.php.net/manual/en/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/php/php-original.svg" alt="lang" />
            <span className="tooltipText">php</span></a>
          </div>
          <div className="tooltip">
            <a href="https://www.typescriptlang.org/docs/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/typescript/typescript-original.svg" alt="lang" />
            <span className="tooltipText">Typescript</span></a>
          </div>
          <div className="tooltip">
            <a href="https://www.javatpoint.com/c-programming-language-tutorial"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/c/c-original.svg" alt="lang" />
            <span className="tooltipText">C</span></a>
          </div>
          <div className="tooltip">
            <a href="https://www.javatpoint.com/cpp-tutorial"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/cplusplus/cplusplus-original.svg" alt="lang" />
            <span className="tooltipText">C++</span></a>
          </div>
        </div>
      </section>
      <section className="knowledge" data-aos="fade-up">
          <h1 class="pltitle">Front-end:</h1>
          <div className="pl">
            <div className="tooltip">
              <a href="https://google.com"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/html5/html5-original-wordmark.svg" alt="lang" />
              <span className="tooltipText">HTML</span></a>
            </div>
            <div className="tooltip">
              <a href="https://google.com"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/figma/figma-original.svg" alt="lang" />
              <span className="tooltipText">Figma</span></a>
            </div>
            <div className="tooltip">
              <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/css3/css3-original-wordmark.svg" alt="lang" />
              <span className="tooltipText">CSS</span></a>
            </div>
            <div className="tooltip">
              <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/javascript/javascript-original.svg" alt="lang" />
              <span className="tooltipText">JS</span></a>
            </div>
            <div className="tooltip">
              <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/bootstrap/bootstrap-original-wordmark.svg" alt="lang" />
              <span className="tooltipText">Bootstrap</span></a>
            </div>
            <div className="tooltip">
              <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/jquery/jquery-original-wordmark.svg" alt="lang" />
              <span className="tooltipText">Jquery</span></a>
            </div>
            <div className="tooltip">
              <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/react/react-original-wordmark.svg" alt="lang" />
              <span className="tooltipText">REACT</span></a>
            </div>
            <div className="tooltip">
              <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/angularjs/angularjs-original.svg" alt="lang" />
              <span className="tooltipText">Angular</span></a>
            </div>
            <div className="tooltip">
              <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/vuejs/vuejs-original-wordmark.svg" alt="lang" />
              <span className="tooltipText">Vue.js</span></a>
            </div>
          </div>
      </section>
      <section className="knowledge" data-aos="fade-up">
          <h1 class="pltitle">Back-end:</h1>
          <div className="pl">
            <div className="tooltip">
              <a href="https://nodejs.org/en/docs/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/nodejs/nodejs-original-wordmark.svg" alt="lang" />
              <span className="tooltipText">Node.js</span></a>
            </div>
            <div className="tooltip">
              <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/php/php-original.svg" alt="lang" />
              <span className="tooltipText">Php</span></a>
            </div>
            <div className="tooltip">
              <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/express/express-original-wordmark.svg" alt="lang" />
              <span className="tooltipText">express.js</span></a>
            </div>
            <div className="tooltip">
              <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/bootstrap/bootstrap-original-wordmark.svg" alt="lang" />
              <span className="tooltipText">Bootstrap</span></a>
            </div>
            <div className="tooltip">
              <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/jquery/jquery-original-wordmark.svg" alt="lang" />
              <span className="tooltipText">Jquery</span></a>
            </div>
            <div className="tooltip">
              <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/react/react-original-wordmark.svg" alt="lang" />
              <span className="tooltipText">REACT</span></a>
            </div>
            <div className="tooltip">
              <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/angularjs/angularjs-original.svg" alt="lang" />
              <span className="tooltipText">Angular</span></a>
            </div>
            <div className="tooltip">
              <a href="https://go.dev/"><img src="https://raw.githubusercontent.com/devicons/devicon/1119b9f84c0290e0f0b38982099a2bd027a48bf1/icons/vuejs/vuejs-original-wordmark.svg" alt="lang" />
              <span className="tooltipText">Vue.js</span></a>
            </div>
          </div>
      </section>
      <section className="footer">
        <div data-aos="flip-left" data-aos-easing="ease-in-sine">
          <SocialIcon id="social" url="https://www.instagram.com/crypto_grapper_/?hl=en" network="instagram"/>
          <SocialIcon id="social" url="https://google.com" network="facebook"/>
          <SocialIcon id="social" url="https://google.com" network="youtube"/>
          <SocialIcon id="social" url="https://google.com" network="github"/>
          <SocialIcon id="social" url="https://www.linkedin.com/in/hariharan-c-0186531aa/" network="linkedin"/>
        </div>
        <div className="rights">
          {/* <h3>© Copyright 2023 <span><a className="company" href="https://intechwings.com" >INTECH&#8734;WINGS</a></span> | All Rights Reserved</h3> */}
          <Copyright></Copyright>
        </div>
      </section>
    </div>
  );
}

export default App;
